from sqlalchemy.orm import Session
from domain.models.models import Employee

def get_employees(db: Session):
    return db.query(Employee).all()

def get_employee_by_id(db: Session, room_id: int):
    return db.query(Employee).filter(Employee.id == room_id).first()

def create_employee(db: Session, employee:Employee):
    employee_checking = db.query(Employee).filter(Employee.email == employee.email).first()
    if employee_checking:
        return "Email already used"
    db.add(employee)
    db.commit()
    db.refresh(employee)
    return employee

def delete_employee_by_id(db: Session, employee_id: int):
    employee = get_employee_by_id(db, employee_id)
    db.delete(employee)
    db.commit()
    return "Employee deleted"

def update_employee(db: Session, employee_id: int, new_employee_data:Employee):
    employee = get_employee_by_id(db, employee_id)
    employee.name = new_employee_data.name
    employee.email = new_employee_data.email
    db.commit()
    db.refresh(employee)
    return employee
