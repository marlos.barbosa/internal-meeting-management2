create table employees
(
	empl_cd_employee serial,
	empl_nm_employee varchar,
	empl_tx_email varchar,
	constraint employees_pkey
		primary key (empl_cd_employee),
	constraint employees_empl_tx_email_key
		unique (empl_tx_email)
);
comment on table employees is 'Table to store employee data';
comment on column employees.empl_cd_employee is 'Employee ID';
comment on column employees.empl_nm_employee is 'Employee name';
comment on column employees.empl_tx_email is 'Employee email';

create table rooms
(
	room_cd_room serial,
	room_nm_room varchar,
	constraint rooms_pkey
		primary key (room_cd_room),
	constraint rooms_room_nm_room_key
		unique (room_nm_room)
);
comment on table rooms is 'Table to store room data';
comment on column rooms.room_cd_room is 'Room ID';
comment on column rooms.room_nm_room is 'Room name';

create table meetings
(
	meet_cd_meeting serial,
	meet_nm_subject varchar,
	meet_dt_start timestamp,
	meet_dt_end timestamp,
	room_cd_room integer,
	constraint meetings_pkey
		primary key (meet_cd_meeting),
	constraint meetings_room_cd_room_fkey
		foreign key (room_cd_room) references rooms
);
comment on table meetings is 'Table to store meeting data';
comment on column meetings.meet_cd_meeting is 'Meeting ID';
comment on column meetings.meet_nm_subject is 'Meeting subject';
comment on column meetings.meet_dt_start is 'Meeting start time';
comment on column meetings.meet_dt_end is 'Meeting end time';

create table meeting_employee
(
	meem_cd_meeting_employee serial,
	meet_cd_meeting integer,
	empl_cd_employee integer,
	constraint meeting_employee_pkey
		primary key (meem_cd_meeting_employee),
	constraint meeting_employee_meet_cd_meeting_empl_cd_employee_key
		unique (meet_cd_meeting, empl_cd_employee),
	constraint meeting_employee_meet_cd_meeting_fkey
		foreign key (meet_cd_meeting) references meetings,
	constraint meeting_employee_empl_cd_employee_fkey
		foreign key (empl_cd_employee) references employees
);
comment on table meeting_employee is 'Table to store meeting-employee relationship';
comment on column meeting_employee.meem_cd_meeting_employee is 'Meeting-employee ID';