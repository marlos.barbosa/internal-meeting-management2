INSERT INTO rooms VALUES ('1','Sala de reuniões 01');
INSERT INTO rooms VALUES ('2','Sala de reuniões 02');

INSERT INTO meetings VALUES ('1','Reunião de planejamento','2024-04-03 09:00:00','2024-04-03 10:00:00',1);
INSERT INTO meetings VALUES ('2','Reunião de retrospectiva','2024-04-03 10:00:00','2024-04-03 11:00:00',1);
INSERT INTO meetings VALUES ('3','Reunião de feedback','2024-04-03 10:00:00','2024-04-03 11:00:00',2);

INSERT INTO employees VALUES ('1','João Carlos','joao.carlos@petrobreizius.com');
INSERT INTO employees VALUES ('2','João Guilherme','joao.guilherme@petrobreizius.com');
INSERT INTO employees VALUES ('3','João Vitor','joao.vitor@petrobreizius.com');

INSERT INTO meeting_employee VALUES ('1','1','1');
INSERT INTO meeting_employee VALUES ('2','1','2');
INSERT INTO meeting_employee VALUES ('3','1','3');

INSERT INTO meeting_employee VALUES ('4','2','1');
INSERT INTO meeting_employee VALUES ('5','2','2');
INSERT INTO meeting_employee VALUES ('6','2','3');

INSERT INTO meeting_employee VALUES ('7','3','1');
INSERT INTO meeting_employee VALUES ('8','3','2');