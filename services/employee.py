from sqlalchemy.orm import Session
from fastapi import HTTPException
from repositories import employee as employee_repository
from domain.schemas.employee import EmployeeCreate
from domain.models.models import Employee

EMPLOYEE_NOT_FOUND = "Employee not found"

def get_employees(db: Session):
    return employee_repository.get_employees(db)

def get_employee_by_id(db: Session, employee_id: int):
    employee = employee_repository.get_employee_by_id(db, employee_id)
    if employee is None:
        raise HTTPException(status_code=404, detail=EMPLOYEE_NOT_FOUND)
    return employee_repository.get_employee_by_id(db, employee_id)

def create_employee(db: Session, employee:EmployeeCreate):
    employee = Employee(**employee.dict())
    return employee_repository.create_employee(db, employee)

def delete_employee_by_id(db: Session, employee_id: int):
    employee = employee_repository.get_employee_by_id(db, employee_id)
    if employee is None:
        raise HTTPException(status_code=404, detail=EMPLOYEE_NOT_FOUND)
    return employee_repository.delete_employee_by_id(db, employee_id)

def update_employee(db: Session, employee_id: int, employee:EmployeeCreate):
    employee_checking = employee_repository.get_employee_by_id(db, employee_id)
    if employee_checking is None:
        raise HTTPException(status_code=404, detail=EMPLOYEE_NOT_FOUND)
    return employee_repository.update_employee(db, employee_id, employee)