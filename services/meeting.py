from sqlalchemy.orm import Session
from repositories import meeting as meeting_repository
from domain.schemas.meeting import MeetingCreate
from domain.models.models import Meeting
from fastapi import HTTPException

MEETING_NOT_FOUND = "Meeting not found"

def get_meetings(db: Session):
    return meeting_repository.get_meetings(db)

def get_meeting_by_id(db: Session, meeting_id: int):
    meeting = meeting_repository.get_meeting_by_id(db, meeting_id)
    if meeting is None:
        raise HTTPException(status_code=404, detail=MEETING_NOT_FOUND)
    return meeting_repository.get_meeting_by_id(db, meeting_id)

def create_meeting(db: Session, meeting:MeetingCreate):
    meeting = Meeting(**meeting.dict())
    return meeting_repository.create_meeting(db, meeting)

def delete_meeting_by_id(db: Session, meeting_id: int):
    meeting = meeting_repository.get_meeting_by_id(db, meeting_id)
    if meeting is None:
        raise HTTPException(status_code=404, detail=MEETING_NOT_FOUND)
    return meeting_repository.delete_meeting_by_id(db, meeting_id)