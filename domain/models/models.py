from domain.models.generic import GenericBase

from sqlalchemy.orm import relationship
from sqlalchemy import Column, Integer, String, ForeignKey, DateTime, UniqueConstraint


class Room(GenericBase):
    __tablename__ = 'rooms'

    id = Column("room_cd_room", Integer, primary_key=True)
    name = Column("room_nm_room", String, unique=True)

    meetings = relationship("Meeting", back_populates="room")


class Meeting(GenericBase):
    __tablename__ = 'meetings'

    id = Column("meet_cd_meeting", Integer, primary_key=True)
    subject:str = Column("meet_nm_subject", String)
    start:DateTime = Column("meet_dt_start", DateTime)
    end:DateTime = Column("meet_dt_end", DateTime)

    room_id = Column("room_cd_room", Integer, ForeignKey('rooms.room_cd_room'))
    room = relationship("Room", back_populates="meetings")

    participants = relationship("Employee", secondary='meeting_employee' , back_populates="meetings")


class Employee(GenericBase):
    __tablename__ = 'employees'

    id = Column("empl_cd_employee", Integer, primary_key=True)
    name = Column("empl_nm_employee", String)
    email = Column("empl_tx_email", String, unique=True)

    meetings = relationship("Meeting", secondary='meeting_employee' , back_populates="participants")


class MeetingEmployee(GenericBase):
    __tablename__ = 'meeting_employee'

    __table_args__ = (
        UniqueConstraint('meet_cd_meeting', 'empl_cd_employee'),
        )

    id = Column("meem_cd_meeting_employee", Integer, primary_key=True)

    meeting_id = Column("meet_cd_meeting", Integer, ForeignKey('meetings.meet_cd_meeting'))
    employee_id = Column("empl_cd_employee", Integer, ForeignKey('employees.empl_cd_employee'))