from datetime import datetime, timedelta
from domain.schemas.generic import GenericModel
from domain.schemas.employee import Employee 
from domain.schemas.room import Room
from pydantic import Field

class MeetingBase(GenericModel):
    subject:str = Field(default="Reunião de alinhamento")
    start:datetime = Field(default=datetime.now())
    end:datetime = Field(default=datetime.now() + timedelta(hours=1))

class MeetingCreate(MeetingBase):
    room_id:int =  Field(default=1)

class Meeting(MeetingBase):
    id: int = Field(default=1)
    room:Room
    participants:list[Employee] = []

    class Config:
        from_attributes = True