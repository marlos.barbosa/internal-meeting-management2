from . import room
from . import employee
from . import meeting
from . import meeting_employee

routes = [
    room.router,
    employee.router,
    meeting.router,
    meeting_employee.router,
]