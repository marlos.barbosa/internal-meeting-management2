from fastapi import APIRouter
from domain.schemas.employee import Employee, EmployeeCreate
from config.database import db_dependency
from services import employee as employee_service

router = APIRouter()
EMPLOYEE_TAG = "Employee data"

@router.get("/employees",
           summary="Fetch all employees",
            tags=[EMPLOYEE_TAG],
            response_model=list[Employee],
            )
def get_employees(db: db_dependency):
    return employee_service.get_employees(db)

@router.get("/employees/{employee_id}",
            summary="Fetch a employee by id",
            tags=[EMPLOYEE_TAG],
            response_model=Employee,
            )
def get_employee_by_id(employee_id: int, db: db_dependency):
    return employee_service.get_employee_by_id(db, employee_id)

@router.delete("/employees/{employee_id}",
               summary="Delete a employee by id",
               tags=[EMPLOYEE_TAG],
               response_model=str,
               )
def delete_employee_by_id(employee_id: int, db: db_dependency):
    return employee_service.delete_employee_by_id(db, employee_id)

@router.post("/employees",
             summary="Create a new employee",
             tags=[EMPLOYEE_TAG],
             response_model=Employee,
             )
def create_room(employee: EmployeeCreate, db: db_dependency):
    return employee_service.create_employee(db, employee)

@router.put("/employees/{employee_id}",
            summary="Update a employee by id",
            tags=[EMPLOYEE_TAG],
            response_model=Employee,
            )
def update_employee(employee_id: int, employee: EmployeeCreate, db: db_dependency):
    return employee_service.update_employee(db, employee_id, employee)