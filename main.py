from fastapi import FastAPI

import uvicorn
from controllers import routes


app = FastAPI(title="Internal Meeting Management Application",
              description="API developed in order to solve issues associated with scheduling of meetings in the chemical restroom cleaning department.",
              version="1.0")

for router in routes:
    app.include_router(router)

if __name__ == '__main__':
    uvicorn.run('main:app', host='localhost', port=8000, reload=True)

